﻿300012 = {
	name = "Knut"
	religion = skaldhyrric_faith
	culture = olavish
	
	diplomacy = 10
	martial = 4
	stewardship = 7
	intrigue = 5
	learning = 12
	prowess = 4
	
	trait = vengeful
	trait = craven
	trait = temperate
	trait = education_learning_3

	965.3.11 = {
		birth = yes
	}
}

300013 = {
	name = "Torbjold"
	religion = skaldhyrric_faith
	culture = olavish
	
	diplomacy = 5
	martial = 12
	stewardship = 6
	intrigue = 2
	learning = 2
	prowess = 14
	
	trait = ambitious
	trait = forgiving
	trait = cynical
	trait = education_martial_2
	trait = intellect_good_1
	trait = scarred
	trait = strategist
	trait = flexible_leader
	
	970 = {
		birth = yes
	}
}

300014 = {
	name = "Brynolf"
	religion = skaldhyrric_faith
	culture = olavish
	
	diplomacy = 8
	martial = 5
	stewardship = 8
	intrigue = 10
	learning = 8
	prowess = 6
	
	trait = calm
	trait = fickle
	trait = paranoid
	trait = education_intrigue_2
	trait = physique_bad_1
	trait = intellect_good_2
	
	999 = {
		birth = yes
	}
}

300015 = {
	name = "Ragnfrid"
	religion = skaldhyrric_faith
	culture = olavish
	female = yes
	
	982 = {
		birth = yes
	}
}

300016 = {
	name = "Ingvar"
	religion = skaldhyrric_faith
	culture = olavish
	
	951 = {
		birth = yes
	}
}

300017 = {
	name = "Bror"
	religion = skaldhyrric_faith
	culture = olavish
	
	972 = {
		birth = yes
	}
}

300018 = {
	name = "Saga"
	religion = skaldhyrric_faith
	culture = olavish
	female = yes
	dna = 300018
	
	diplomacy = 6
	martial = 8
	stewardship = 9
	intrigue = 6
	learning = 4
	prowess = 10
	
	trait = diligent
	trait = chaste
	trait = gregarious
	trait = education_stewardship_2
	trait = shieldmaiden
	trait = lifestyle_hunter
	
	1001 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			change_current_weight = {value = current_weight multiply = -1}
			change_target_weight = {value = target_weight multiply = -1}
			set_sexuality = homosexual
		}
	}
}

###############
# Dynasty Fjorhavn

fjorhavn_0005 = { # baron of Fjorhavn
	name = "Erik"
	dynasty = dynasty_fjorhavn
	religion = skaldhyrric_faith
	culture = olavish

	trait = race_human

	1000.04.02 = {
		birth = yes
	}
}