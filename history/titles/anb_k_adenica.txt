﻿k_adenica = {
	1000.1.1 = { change_development_level = 8 }
}

d_adenica = {
	1017.09.13 = {
		holder = escanni_0037
		government = feudal_government
	}
}

c_balgarton = {
	1000.1.1 = { change_development_level = 10 }
	1017.09.13 = {
		holder = escanni_0038
	}
	1017.09.13 = {
		liege = d_adenica
	}
}

c_tiltwick = {
	1000.1.1 = { change_development_level = 11 }
	1017.09.13 = {
		holder = escanni_0037
	}
}

c_upcreek = {
	1000.1.1 = { change_development_level = 11 }
	1017.09.13 = {
		holder = escanni_0039
	}
	1017.09.13 = {
		liege = d_adenica
	}
}

d_falsemire = {
	1017.09.13 = {
		holder = escanni_0037
	}
}

c_mireleigh = {
	1000.1.1 = { change_development_level = 7 }
	926.03.21 = {
		holder = escanni_0046
	}
	
	1017.09.13 = {
		holder = escanni_0044
	}
	1017.09.13 = {
		liege = d_adenica
	}
}

c_falseharbour = {
	1000.1.1 = { change_development_level = 7 }
	926.03.21 = {
		holder = escanni_0046
	}
	
	1017.09.13 = {
		holder = escanni_0044
	}
	1017.09.13 = {
		liege = d_adenica
	}
}

c_listfields = {
	1000.1.1 = { change_development_level = 7 }
	1017.09.13 = {
		holder = escanni_0043
	}
	1017.09.13 = {
		liege = d_adenica
	}
}

d_taran_plains = {
	1000.1.1 = { change_development_level = 7 }
	1002.01.01 = {
		holder = escanni_0050
	}
}

c_taranton = {
	1000.1.1 = { change_development_level = 7 }
	1002.01.01 = {
		holder = escanni_0050
	}
	
}

c_cantercurse = {
	1000.1.1 = { change_development_level = 7 }
	1002.01.01 = {
		holder = escanni_0058
	}
	1012.01.12 = {
		liege = d_taran_plains
	}
}

c_banwick = {
	1000.1.1 = { change_development_level = 7 }
	1002.01.01 = {
		holder = escanni_0055
	}
	1012.01.12 = {
		liege = d_taran_plains
	}
}

d_rohibon = {
	1000.1.1 = { change_development_level = 7 }
}

c_rohibon = {
	1000.1.1 = { change_development_level = 7 }
	1003.04.05 = {
		holder = escanni_0062
	}
}

c_halansar = {
	1000.1.1 = { change_development_level = 7 }
	1003.04.05 = {
		holder = escanni_0062
	}
}

d_acengard = {
	1010.10.31 = {
		holder = 60014
	}
}

c_acengard = {
	1000.1.1 = { change_development_level = 9 }
	1010.10.31 = {
		holder = 60014
	}
}

c_carlanhal = {
	1000.1.1 = { change_development_level = 13 }
	1010.10.31 = {
		liege = d_acengard
	}
}

c_taranton = {
	1000.1.1 = { change_development_level = 11 }
	1010.10.31 = {
		liege = d_acengard
	}
}

d_valefort = {
	1016.7.8 = {
		holder = 76 #Martin Farran
		government = feudal_government
	}
}

c_valefort = {
	1000.1.1 = { change_development_level = 7 }
}

d_verteben = {
	1015.9.27 = {
		holder = 121 #Henric Verteben
	}
	1020.9.30 = {
		holder = 122 #Edith the Smith
	}
}

c_shieldrest = {
	1000.1.1 = { change_development_level = 7 }
}

c_gallopsway={
	1021.9.30 = {
		holder = escanni_0068 #Tomac of Gallopsway
	}
}