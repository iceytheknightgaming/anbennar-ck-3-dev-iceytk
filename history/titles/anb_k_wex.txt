﻿k_wex = {
	1000.1.1 = { change_development_level = 8 }
}

d_wexhills = {
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

c_wexkeep = {
	1000.1.1 = { change_development_level = 12 }
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

c_threeguard = {
	1000.1.1 = { change_development_level = 8 }
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

d_ottocam = {
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

c_ottocam={
	960.1.1 = {
		holder = ottocam_0001 #Ottrac Ottocam
	}
	978.3.18 = {# placeholder
		holder = 0
	}
}

c_vinerick = {
	1000.1.1 = { change_development_level = 9 }
}

c_indlebury = {
	1000.1.1 = { change_development_level = 9 }
}

d_bisan = {
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_bisan = {
	1000.1.1 = { change_development_level = 9 }
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_magdalaire = {
	1000.1.1 = { change_development_level = 9 }
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

d_greater_bardswood = {
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_bardswood = {
	1000.1.1 = { change_development_level = 7 }
}

c_stumpwood = {
	1000.1.1 = { change_development_level = 7 }
}

d_sugamber = {
	970.3.4 = {
		holder = 66 #Rycroft Sugambic
	}
}

c_rhinmond = {
	1000.1.1 = { change_development_level = 9 }
}

d_escandar = {
	1000.1.1 = { change_development_level = 6 }
	1017.1.1 = {
		holder = 551 #Magda asil Magda
	}
}

c_escandar = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

c_gnollsgate = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

c_rupellion = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

b_aelthil = {
	1015.3.21 = {
		holder = 552 #Celarion, Magda's elven husband
	}
}

b_amberflow = {
	1015.6.7 = {
		holder = moonbeam0001 #Barry, 63
	}
}