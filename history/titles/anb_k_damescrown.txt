k_damescrown = {
	1000.1.1 = { change_development_level = 8 }
}

d_damescrown = {
	1000.1.1 = { change_development_level = 12 }
	1021.7.19 = {
		holder = 77 #Damris Eldman
		government = republic_government
	}
}

c_damescrown = {
	1000.1.1 = { change_development_level = 21 }
}

c_aramar = {
	1000.1.1 = { change_development_level = 13 }
}

d_crodam = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

c_crodam = {
	1000.1.1 = { change_development_level = 9 }
}

c_derwing = {
	1000.1.1 = { change_development_level = 10 }
}

c_wrothcross = {
	1000.1.1 = { change_development_level = 9 }
}

c_marcestir = {
	1000.1.1 = { change_development_level = 9 }
	1020.1.2 = {
		holder = 77 #Damris Eldman
	}
}

c_elfsmithy = {
	1000.1.1 = { change_development_level = 7 }
	1020.1.2 = {
		holder = 77 #Damris Eldman
	}
}

d_teagansfield = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

c_teagansfield = {
	1000.1.1 = { change_development_level = 9 }
	1020.10.1 = {
		holder = crownsman0001
		liege = k_arbaran
	}
}

c_casnamoine = {
	1000.1.1 = { change_development_level = 7 }
}

d_aranmas = {
	1000.1.1 = { change_development_level = 10 }
	1005.7.3 = {
		holder = 69 #Laren Estallen
		government = feudal_government
	}
}

c_calascandar = {
	1000.1.1 = { change_development_level = 11 }
	1020.4.5 = {
		holder = 22 #Calasandur
		government = feudal_government
		liege = "k_arbaran"
	}
}