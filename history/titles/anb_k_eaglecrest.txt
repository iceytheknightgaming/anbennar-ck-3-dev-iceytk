k_eaglecrest = {
	1000.1.1 = {
		change_development_level = 8
	}
	1010.4.2 = {
		holder = 56 #Warde Eaglecrest
	}
}

c_eaglecrest = {
	1000.1.1 = {
		change_development_level = 10
	}
}

c_lodan_crags = {
	827.1.1 = { 
		liege = k_eaglecrest
	}
	975.1.2 = {
		holder = lodan_0002 #Carl Lodan
	}
	1000.1.1 = {
		change_development_level = 6
	}
	1007.5.15 = {
		holder = lodan_0001 #Humbar Lodan
	}
}

c_feycombe = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_south_floodmarsh = {
	1010.5.5 = {
		liege = k_eaglecrest
	}
	975.1.1 = {
		holder = floodmark_0001 #Conric Floodmark
	}
	1000.1.1 = {
		change_development_level = 6
	}
}

c_north_floodmarsh = {
	1000.1.1 = {
		change_development_level = 6
	}
}
