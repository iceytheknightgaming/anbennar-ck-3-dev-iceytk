#k_iochand
##d_iochand
###c_iochand
142 = {		#Iochand

    # Misc
    holding = church_holding

    # History

}
1352 = {

    # Misc
    culture = iochander
    religion = iochand_cult
    holding = castle_holding

    # History

}
1354 = {

    # Misc
    holding = none

    # History

}
1355 = {

    # Misc
    holding = city_holding

    # History

}
1356 = {

    # Misc
    holding = none

    # History

}

###c_eldergreen
129 = {		#Eldergreen

    # Misc
    culture = iochander
    religion = iochand_cult
	holding = castle_holding

    # History
}
1342 = {

    # Misc
    holding = none

    # History

}

###c_carverhold
143 = {		#Carverhold

    # Misc
    culture = iochander
    religion = iochand_cult
	holding = castle_holding

    # History
}
1357 = {

    # Misc
    holding = city_holding

    # History

}
1358 = {

    # Misc
    holding = none

    # History

}

##d_portnamm
###c_portnamm
126 = {

    # Misc
    culture = creek_gnomish
    religion = iochand_cult
	holding = city_holding

    # History
}
2695 = {

    # Misc
    holding = city_holding

    # History

}
2696 = {

    # Misc
    holding = none

    # History

}
2697 = {    #THONK

    # Misc
    holding = church_holding

    # History

}

##d_southroy
###c_southroy
128 = {

    # Misc
    culture = iochander
    religion = iochand_cult
	holding = castle_holding

    # History
}
1341 = {

    # Misc
    holding = none

    # History

}
1340 = {

    # Misc
    holding = none

    # History

}
1339 = {

    # Misc
    holding = city_holding

    # History

}

###c_haysfield
125 = {		#Haysfield

    # Misc
    culture = iochander
    religion = iochand_cult
	holding = castle_holding

    # History
}
1430 = {

    # Misc
    holding = none

    # History

}
1432 = {

    # Misc
    holding = city_holding

    # History

}

###c_thunderward
123 = {		#Thunderward

    # Misc
    culture = iochander
    religion = iochand_cult
	holding = castle_holding

    # History
}
1245 = {

    # Misc
    holding = none

    # History

}

###c_sildthol
124 = {		#Sildthol

    # Misc
    culture = iochander
    religion = iochand_cult
	holding = castle_holding

    # History
}
1337 = {

    # Misc
    holding = city_holding

    # History

}
1338 = {

    # Misc
    holding = none

    # History

}
