﻿### Forbidden Magic ###

# Gives a secret if not already present
forbidden_magic_secret_effect = {
	if = {
		limit = { 
			NAND = {
				any_secret = { secret_type = secret_forbidden_magic }
				has_trait = forbidden_magic_practitioner
			} 
		}
		add_secret = { type = secret_forbidden_magic }
	}
}

# Exposes secret and rewards trait
gain_forbidden_magic_trait_effect = {
	add_trait = forbidden_magic_practitioner	# Invalidates the Secret automatically
	remove_piety_level_for_shunned_or_criminal_trait_effect = { TRAIT = forbidden_magic_practitioner }
}

### Spells ###

# Compel #

# Gives a weak "Compel" hook
compel_success_effect = {
	scope:owner = {
		add_hook_no_toast = {
			type = compel_hook
			target = scope:target
		}
		forbidden_magic_secret_effect = yes
	}
}

# Thy dabbling in the dark arts shall never be forgotten
compel_failure_effect = {
	forbidden_magic_secret_effect = yes
}


# Opinion Loss and Secret revelation
compel_discovery_effect = {
	# Opinion Malus with the Victim
		scope:target = {
			add_opinion = {
				modifier = attempted_forbidden_magic_opinion
				target = scope:owner
			}
			remove_positive_relation_effect = { TARGET = scope:owner }
			progress_towards_rival_effect = {
				CHARACTER = scope:owner
				REASON = tried_to_compel
				OPINION = 0
			}
		}
	# Exposure
	gain_forbidden_magic_trait_effect = yes
}

# Dominate #

# Gives a strong "Dominate" hook
dominate_success_effect = {
	# Breaks the hold somebody else might have over their mind
	hidden_effect = {
		scope:target.var:controller = {
			remove_hook = {
				type = dominate_hook
				target = scope:target
			}
			trigger_event = anb_dominate_outcome.5
		}
	}
	# Gets a strong hook and is saved as (new) controller
	add_hook_no_toast = {
		type = dominate_hook
		target = scope:target
	}	
	forbidden_magic_secret_effect = yes
	scope:target = {
		set_variable = {
			name = controller
			value = scope:owner
		}
		trigger_event = anb_dominate_victim.1
	}
}

# Thy dabbling in the dark arts shall never be forgotten
dominate_failure_effect = { 
	forbidden_magic_secret_effect = yes
}


# Opinion Loss and Secret revelation
dominate_discovery_effect = {
	# Opinion Malus with the Victim
		scope:target = {
			add_opinion = {
				modifier = attempted_forbidden_magic_opinion
				opinion = -50
				target = scope:owner
			}
			remove_positive_relation_effect = { TARGET = scope:owner }
			progress_towards_rival_effect = {
				CHARACTER = scope:owner
				REASON = tried_to_dominate
				OPINION = 0
			}
		}
	# Exposure
	gain_forbidden_magic_trait_effect = yes
}