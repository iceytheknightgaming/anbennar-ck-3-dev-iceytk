﻿###DECISIONS LIST###

#unite_the_reach_decision

unite_the_reach_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_realm.dds"
	major = yes
	ai_goal = yes
	desc = unite_the_reach_decision_desc
	
	cost = {
		gold = 1000
		prestige = 1000
	}

	is_shown = {
		culture = {
			has_cultural_pillar = language_reachman_common
		}
		#Both Adshaw and Vrorenmarch have de jure land
		title_has_de_jure = { TITLE = title:k_adshaw }
		title_has_de_jure = { TITLE = title:k_vrorenmarch }
		NOT = {
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:unite_the_reach_decision
			}
		}
		highest_held_title_tier < tier_empire
	}

	is_valid = {
		completely_controls = title:k_adshaw
		has_title = title:k_adshaw
		
		completely_controls = title:k_vrorenmarch
		has_title = title:k_vrorenmarch
		
		OR = {
			has_primary_title = title:k_adshaw
			has_primary_title = title:k_vrorenmarch
		}
		
		prestige_level >= 4
	}
	
	is_valid_showing_failures_only = {
		is_capable_adult = yes
		is_imprisoned = no
		is_independent_ruler = yes
		is_at_war = no
	}

	effect = {
		save_scope_as = the_reach_former

		show_as_tooltip = { unite_the_reach_decision_effect = yes } #Actually applied in anb_decision_major_events.0003 - prestige, laws, title changes

		#Events
		trigger_event = anb_decision_major_events.0003
		every_player = {
			limit = {
				NOT = { this = scope:the_reach_former }
				is_within_diplo_range = { CHARACTER = scope:the_reach_former }
			}
			trigger_event = anb_decision_major_events.0004
		}

		#Can only be done once
		add_to_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:unite_the_reach_decision
		}
		set_global_variable = {
			name = unite_the_reach_decision
			value = scope:the_reach_former
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

#bring_the_reach_into_the_fold_decision

bring_the_reach_into_the_fold_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_realm.dds"
	major = yes
	ai_check_interval = 120
	desc = bring_the_reach_into_the_fold_decision_desc

	is_shown = {
		has_title = title:e_gerudia
		has_primary_title = title:e_gerudia
		OR = {
			title_is_valid_to_move_to_gerudia = {
				TITLE = title:k_adshaw
			}
			title_is_valid_to_move_to_gerudia = {
				TITLE = title:k_vrorenmarch
			}
		}
	}

	is_valid = {
		has_title = title:e_gerudia
		OR = {
			can_move_title_into_gerudia_trigger = {
				TITLE = title:k_adshaw
			}
			can_move_title_into_gerudia_trigger = {
				TITLE = title:k_vrorenmarch
			}
		}
	}
	
	is_valid_showing_failures_only = {
		is_capable_adult = yes
		is_imprisoned = no
		is_independent_ruler = yes
		is_at_war = no
	}

	effect = {
		move_all_valid_reach_kingdoms_effect = yes
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}
