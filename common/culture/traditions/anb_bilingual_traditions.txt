﻿tradition_language_gnomish_lencori = {
	category = societal

	layers = {
		0 = intrigue
		1 = indian
		4 = philosopher.dds
	}
	
	parameters = {
		languages_speed_up_claims = yes
		rulers_want_to_learn_languages_of_coreligionists = yes
		charaters_are_bilingual = yes
		extra_language_cap = yes
	}
	character_modifier = {
		learn_language_scheme_power_mult = 0.25
	}
	is_shown = {
		any_parent_culture_or_above = {
			has_cultural_pillar = language_gnomish
		}
		any_parent_culture_or_above = {
			has_cultural_pillar = language_lencori_common
		}
	}
	can_pick = {
		OR = {
			has_cultural_pillar = language_gnomish
			has_cultural_pillar = language_lencori_common
		}
	}
	
	# TODO - you must have one of the languages as the cultures language
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						culture_pillar:ethos_spiritual = { is_in_list = traits }
						culture_pillar:ethos_bureaucratic = { is_in_list = traits }
						culture_pillar:ethos_egalitarian = { is_in_list = traits }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bureaucratic_spiritual_or_egalitarian_desc
				}
			}
			if = {
				limit = {
					NOT = {
						scope:character = {
							knows_language = language_lencori_common
							knows_language = language_gnomish
						}
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = not_know_langauges_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	ai_will_do = {
		value = 100
	}
}

tradition_language_castanorian_dwarven = {
	category = societal

	layers = {
		0 = intrigue
		1 = indian
		4 = philosopher.dds
	}
	
	parameters = {
		charaters_are_bilingual = yes
		extra_language_cap = yes
	}
	
	# TODO - other stuff?
	
	is_shown = {
		any_parent_culture_or_above = {
			has_cultural_pillar = language_cannorian_dwarven
		}
		any_parent_culture_or_above = {
			has_cultural_pillar = language_castanorian_common
		}
	}
	can_pick = {
		OR = {
			has_cultural_pillar = language_cannorian_dwarven
			has_cultural_pillar = language_castanorian_common
		}
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						culture_pillar:ethos_spiritual = { is_in_list = traits }
						culture_pillar:ethos_bureaucratic = { is_in_list = traits }
						culture_pillar:ethos_egalitarian = { is_in_list = traits }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bureaucratic_spiritual_or_egalitarian_desc
				}
			}
			if = {
				limit = {
					NOT = {
						scope:character = {
							knows_language = language_castanorian_common
							knows_language = language_cannorian_dwarven
						}
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = not_know_langauges_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	ai_will_do = {
		value = 100
	}
}

tradition_language_copper_dwarven_bahari = {
	category = societal

	layers = {
		0 = intrigue
		1 = indian
		4 = philosopher.dds
	}
	
	parameters = {
		charaters_are_bilingual = yes
		extra_language_cap = yes
	}
	
	# TODO - other stuff?
	
	is_shown = {
		any_parent_culture_or_above = {
			has_cultural_pillar = language_bahari
		}
		any_parent_culture_or_above = {
			has_cultural_pillar = language_segbandalic
		}
	}
	can_pick = {
		OR = {
			has_cultural_pillar = language_bahari
			has_cultural_pillar = language_segbandalic
		}
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						culture_pillar:ethos_spiritual = { is_in_list = traits }
						culture_pillar:ethos_bureaucratic = { is_in_list = traits }
						culture_pillar:ethos_egalitarian = { is_in_list = traits }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bureaucratic_spiritual_or_egalitarian_desc
				}
			}
			if = {
				limit = {
					NOT = {
						scope:character = {
							knows_language = language_bahari
							knows_language = language_segbandalic
						}
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = not_know_langauges_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	ai_will_do = {
		value = 100
	}
}