﻿name_list_kheteratan = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {

		#From EU4
		Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
		Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
		Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
		Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
		Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
	}
	female_names = {
		Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
		Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
		Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_deshaki = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {

		#From EU4
		Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
		Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
		Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
		Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
		Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
	}
	female_names = {
		Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
		Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
		Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_ekhani = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {

		#From EU4
		Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
		Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
		Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
		Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
		Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
	}
	female_names = {
		Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
		Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
		Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_khasani = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {

		#From EU4
		Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
		Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
		Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
		Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
		Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
	}
	female_names = {
		Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
		Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
		Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}
