﻿corvurian = {
	color = { 120 120 120 }
	created = 800.1.1
	parents = { korbarid castanorian }	#cos C

	ethos = ethos_courtly
	heritage = heritage_dostanorian
	language = language_borders_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_isolationist
		tradition_ruling_caste
		tradition_castle_keepers
		tradition_hereditary_hierarchy
	}
	
	name_list = name_list_corvurian
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		10 = slavic_blond
		5 = slavic_ginger
		10 = slavic_brown_hair
		80 = slavic_dark_hair
	}
}

korbarid = {
	color = { 52 52 52 }

	ethos = ethos_stoic
	heritage = heritage_dostanorian
	language = language_korbarid
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_isolationist
		tradition_forest_folk
		tradition_tribe_unity
	}
	dlc_tradition = {
		trait = tradition_staunch_traditionalists
		requires_dlc_flag = hybridize_culture
	}
	
	name_list = name_list_korbarid
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = slavic_northern_brown_hair
		80 = slavic_northern_dark_hair
	}
}

ourdi = {
	color = { 90 120 60 }
	created = 400.1.1
	parents = { corvurian yametsesi }

	ethos = ethos_stoic
	heritage = heritage_dostanorian
	language = language_castanorian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_castle_keepers
		tradition_by_the_sword
		tradition_stand_and_fight
		tradition_zealous_people
	}
	
	name_list = name_list_corvurian
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		10 = slavic_brown_hair
		60 = slavic_dark_hair
		30 = mediterranean_byzantine	#this is used in bahari and gelkar type
	}
}

cardesti = {
	color = { 182 131 46 }

	ethos = ethos_communal
	heritage = heritage_cardesti
	language = language_cardesti
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_diasporic
		tradition_music_theory
		tradition_festivities
		tradition_caravaneers
	}
	
	name_list = name_list_cardesti
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		8 = indian
		2 = mediterranean_byzantine
	}
}

devacedi = {
	color = { 206 139 101 }
	
	parents = { korbarid marcher }
	
	created = 671.11.11

	ethos = ethos_stoic
	heritage = heritage_dostanorian
	language = language_castanorian_common
	martial_custom = martial_custom_equal
	traditions = {
		tradition_zealous_people
		tradition_martial_admiration
		tradition_tribe_unity
		tradition_stalwart_defenders
		
	}
	
	name_list = name_list_castanorian
	name_list = name_list_korbarid
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		10 = slavic_blond
		5 = slavic_ginger
		30 = slavic_brown_hair
		30 = slavic_dark_hair
	}
}

treocid = {
	color = { 75 127 81 }

	ethos = ethos_spiritual
	heritage = heritage_dostanorian
	language = language_korbarid
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_forest_folk
		tradition_sacred_groves
		tradition_staunch_traditionalists
		
	}
	
	name_list = name_list_castanorian
	name_list = name_list_korbarid
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		10 = slavic_blond
		5 = slavic_ginger
		30 = slavic_brown_hair
		30 = slavic_dark_hair
	}
}