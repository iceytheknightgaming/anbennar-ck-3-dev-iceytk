﻿zanite = {
	color = { 215 100 115 }
	
	ethos = ethos_egalitarian
	heritage = heritage_bulwari
	language = language_zanite
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_garden_of_surakes
		tradition_city_keepers
		tradition_legalistic
	}
	dlc_tradition = {
		trait = tradition_artisans
		requires_dlc_flag = royal_court
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

akalsesi = {
	color = { 35 40 50 }
	
	ethos = ethos_courtly
	heritage = heritage_bulwari
	language = language_zanite
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_garden_of_surakes
		tradition_equitable
		tradition_horse_breeder
		tradition_only_the_strong
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

brasanni = {
	color = { 205 235 230 }
	
	ethos = ethos_egalitarian
	heritage = heritage_bulwari
	language = language_barsuyitu
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_garden_of_surakes
		tradition_city_keepers
		tradition_maritime_mercantilism
		tradition_talent_acquisition
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

gelkar = {
	color = { 55 185 200 }
	
	ethos = ethos_stoic
	heritage = heritage_bulwari
	language = language_elumite
	martial_custom = martial_custom_equal
	traditions = {
		tradition_loyal_soldiers
		tradition_mountain_herding
		tradition_mountaineers
		tradition_polygamous
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		25 = arab
		75 = mediterranean_byzantine
	}
}

bahari = {
	color = { 125 215 90 }
	
	ethos = ethos_stoic
	heritage = heritage_bulwari
	language = language_bahari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_forest_folk
		tradition_forest_fighters
		tradition_maritime_mercantilism
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		50 = arab
		50 = mediterranean_byzantine
	}
}

yametsesi = {
	color = { 250 145 40 }
	
	ethos = ethos_spiritual
	heritage = heritage_bulwari
	language = language_bahari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_seafaring
		tradition_castle_keepers
		tradition_maritime_mercantilism
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		40 = arab
		60 = mediterranean_byzantine
	}
}

surani = {
	color = { 245 205 135 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_surani
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_garden_of_surakes
		tradition_fervent_temple_builders
		tradition_dryland_dwellers
		tradition_religious_patronage
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

sadnatu = {
	color = { 60 45 10 }
	created = 502.1.1
	parents = { barsibu }
	
	ethos = ethos_stoic
	heritage = heritage_bulwari
	language = language_barsuyitu
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hill_dwellers
		tradition_metal_craftsmanship
		tradition_highland_warriors
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

masnsih = {
	color = { 200 105 50 }
	created = 488.1.1
	parents = { zanite }
	
	ethos = ethos_communal
	heritage = heritage_bulwari
	language = language_masnsih
	martial_custom = martial_custom_equal
	traditions = {
		tradition_caravaneers
		tradition_warriors_of_the_dry
		tradition_desert_nomads
		tradition_warrior_culture
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

maqeti = {
	color = { 190 190 50 }
	created = 930.1.1
	parents = { zanite }
	
	ethos = ethos_bellicose
	heritage = heritage_bulwari
	language = language_zanite
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_fishermen
		tradition_martial_admiration
		tradition_faith_bound
		tradition_isolationist
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

barsibu = {
	color = { 45 35 40 }
	
	ethos = ethos_courtly
	heritage = heritage_bulwari
	language = language_barsuyitu
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_garden_of_surakes
		tradition_city_keepers
		tradition_zealous_people
	}
	dlc_tradition = {
		trait = tradition_artisans
		requires_dlc_flag = royal_court
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

drolasesi = {
	color = { 145 30 50 }
	
	ethos = ethos_egalitarian
	heritage = heritage_bulwari
	language = language_barsuyitu
	martial_custom = martial_custom_equal
	traditions = {
		tradition_practiced_pirates
		tradition_seafaring
		tradition_talent_acquisition
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

kuzarami = {
	color = { 55 65 100 }
	
	ethos = ethos_communal
	heritage = heritage_bulwari
	language = language_elumite
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hill_dwellers
		tradition_storytellers
		tradition_adaptive_skirmishing
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

ilatani = {
	color =  { 59 148 235 }
	
	ethos = ethos_courtly
	heritage = heritage_bulwari
	language = language_businori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_city_keepers
		tradition_poetry
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		20 = arab
		80 = mediterranean_byzantine
	}
}

east_divenori = {
	color = { 141 141 70 }
	created = 897.1.1
	parents = { dalric yametsesi }
	
	ethos = ethos_bellicose
	heritage = heritage_gerudian
	language = language_bahari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_quarrelsome
		tradition_practiced_pirates
	}
	dlc_tradition = {
		trait = tradition_fp1_coastal_warriors
		requires_dlc_flag = the_northern_lords
		fallback = tradition_hird
	}
	dlc_tradition = {
		trait = tradition_fp1_performative_honour
		requires_dlc_flag = the_northern_lords
	}
	
	
	name_list = name_list_dalric
				

	coa_gfx = { norse_coa_gfx arabic_group_coa_gfx } 
	building_gfx = { mena_building_gfx } 
	clothing_gfx = { fp1_norse_clothing_gfx northern_clothing_gfx dde_abbasid_clothing_gfx mena_clothing_gfx } 
	unit_gfx = { northern_unit_gfx } 		

	ethnicities = {
		20 = arab
		20 = mediterranean_byzantine
		30 = caucasian_northern_blond
		5 = caucasian_northern_ginger
		15 = caucasian_northern_brown_hair
		10 = caucasian_northern_dark_hair
	}
}