﻿deshaki = {
	color = { 50 165 195 }

	ethos = ethos_courtly
	heritage = heritage_akasi
	language = language_deshaki
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_sacred_mountains
		tradition_ancient_miners
		tradition_welcoming
		tradition_castle_keepers
	}
	
	name_list = name_list_deshaki
	
	coa_gfx = { berber_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}

ekhani = {
	color = { 206 222 199 }

	ethos = ethos_spiritual
	heritage = heritage_akasi
	language = language_ekhani
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_sacred_mountains
		tradition_republican_legacy
		tradition_maritime_mercantilism
		tradition_seafaring
	}
	
	name_list = name_list_ekhani
	
	coa_gfx = { berber_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}

khasani = {
	color = { 90 135 65 }

	ethos = ethos_courtly
	heritage = heritage_akasi
	language = language_khasani
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_sacred_mountains
		tradition_xenophilic
		tradition_dryland_dwellers
		tradition_mountaineers
	}
	
	name_list = name_list_khasani
	
	coa_gfx = { berber_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}