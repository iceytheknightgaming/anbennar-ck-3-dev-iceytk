﻿# standard costs
@maa_buy_cost = 150
@maa_low_maintenance_cost = 1.0
@maa_high_maintenance_cost = 5.0
@cultural_maa_extra_ai_score = 80 # Equivalent to having 8 extra regiments beyond what the code scoring would indicate (see NEGATIVE_SCORE_PER_EXISTING_REGIMENT)

adventurers = {
	type = heavy_infantry

	damage = 40
	toughness = 20
	pursuit = 0
	screen = 24	
	counters = {
		pikemen = 1
	}

	can_recruit = {
		always = no # spawned via event
	}
	
	buy_cost = { gold = heavy_infantry_recruitment_cost }
	low_maintenance_cost = { gold = heavy_infantry_low_maint_cost }
	high_maintenance_cost = { gold = heavy_infantry_high_maint_cost }
	
	stack = 50
	ai_quality = { value = @cultural_maa_extra_ai_score }
	icon = heavy_infantry
}
