﻿
# #Cultures that are elvenized, used within culture scope
culture_is_elvenized_trigger = {
	has_innovation = innovation_elvenization
}

#Requirements for elvenization innovation
can_adopt_elvenization_innovation = {
	OR = {
		# Elven cultures since they should always have it
		has_cultural_pillar = heritage_elven
		# Culture has innovation
		culture_is_elvenized_trigger = yes
		# Parent culture has innovation
		any_parent_culture_or_above = {
			OR = {
				has_cultural_pillar = heritage_elven
				culture_is_elvenized_trigger = yes
			}
		}
		# Culture head, its spouse, its capital or a councillor is elf or half-elf
		culture_head ?= {
			OR = {
				has_trait = race_elf
				has_trait = race_half_elf
				primary_spouse ?= {
					OR = {
						has_trait = race_elf
						has_trait = race_half_elf
					}
				}
				any_councillor = {
					is_powerful_vassal = yes
					OR = {
						has_trait = race_elf
						has_trait = race_half_elf
					}
				}
				capital_province.culture ?= {
					OR = {
						has_cultural_pillar = heritage_elven
						culture_is_elvenized_trigger = yes
					}
				}
			}
		}
	}
}
