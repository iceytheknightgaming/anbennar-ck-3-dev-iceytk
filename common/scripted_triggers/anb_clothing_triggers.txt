﻿
portrait_elvish_clothing_trigger = {
	scope:culture = { portrait_elvish_clothing_contents_trigger = yes }
}

portrait_elvish_clothing_spouse_trigger = {
	culture = { portrait_elvish_clothing_contents_trigger = yes }
}

portrait_elvish_clothing_contents_trigger = {
	has_clothing_gfx = elvish_clothing_gfx
}

portrait_kheteratan_clothing_trigger = {
	scope:culture = { portrait_kheteratan_clothing_contents_trigger = yes }
}

portrait_kheteratan_clothing_spouse_trigger = {
	culture = { portrait_kheteratan_clothing_contents_trigger = yes }
}

portrait_kheteratan_clothing_contents_trigger = {
	has_clothing_gfx = kheteratan_clothing_gfx
}

portrait_kheteratan_clothing_spouse_trigger = {
	culture = { portrait_kheteratan_clothing_contents_trigger = yes }
}