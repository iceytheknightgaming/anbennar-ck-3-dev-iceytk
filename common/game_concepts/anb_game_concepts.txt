﻿magic_mastery = {
	parent = skills
	alias = { magical_mastery magic_mastery_i }

	texture = "gfx/interface/icons/icon_magic_mastery.dds"
}

court_mage = {
	parent = councillor
	alias = { court_mages }
}

war_of_the_sorcerer_king = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_balmire = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_morban_flats = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_trialmount = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_red_reach = {
	texture = "gfx/interface/icons/map_icons/combat_map_icon.dds"
}

racial_legitimacy = {
	alias = { legitimate_race illegitimate_race }
}

calindal = {
}

bladestewards = {
	alias = { bladesteward order_of_bladestewards }
}

quest = {
	alias = { quests }
}